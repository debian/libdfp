libdfp (1.0.16-1) unstable; urgency=medium

  * Update to new upstream version 1.0.16.

 -- Frédéric Bonnard <frediz@debian.org>  Wed, 20 Oct 2021 12:14:41 +0200

libdfp (1.0.15-2) unstable; urgency=medium

  * Fix symbol file for s390x

 -- Frédéric Bonnard <frediz@debian.org>  Tue, 28 Jul 2020 12:11:22 +0200

libdfp (1.0.15-1) unstable; urgency=medium

  * Update to new upstream version 1.0.15.
  * Add amd64 support following upstream move
  * Provide pkgconfig files
  * Update d/copyright file according to upstream changes
  * Add i386 support following upstream move
  * Migrate to compatibility level 13
  * Deal with documentation files
  * Update symbol file
  * Add Rules-Requires-Root field
  * Remove useless install_root
  * Fix .pc permissions
  * Fix reproducibility issue
  * Bump Standards-Version
  * Depend on architectures for d/*.symbols

 -- Frédéric Bonnard <frediz@debian.org>  Tue, 28 Jul 2020 11:17:20 +0200

libdfp (1.0.14-2) unstable; urgency=medium

  [ Dimitri John Ledkov ]
  * Remove myself from uploaders.

 -- Frédéric Bonnard <frediz@debian.org>  Mon, 12 Aug 2019 16:32:48 +0200

libdfp (1.0.14-1) unstable; urgency=medium

  * New upstream release
  * Remove patch on test/test-amort.c
  * Remove patch 0002-Adapt-the-code-for-glibc-2.28.patch
  * Remove patch 0003-Return-the-number-of-chars-output-in-printf.patch
  * Rename 0004-Use-python3.patch
  * Switch to debhelper-compat usage
  * Bump Standards-Version
  * Ignore tests known to fail on soft-dfp and POWER hard-dfp

 -- Frédéric Bonnard <frediz@debian.org>  Mon, 12 Aug 2019 11:42:18 +0200

libdfp (1.0.13-3) unstable; urgency=medium

  * Fix 2 FTBFS.
    Thanks to Steve Langasek for the bug report, and test on Ubuntu
    (Closes: #921119)

 -- Frédéric Bonnard <frediz@debian.org>  Mon, 04 Feb 2019 15:36:11 +0100

libdfp (1.0.13-2) unstable; urgency=medium

  * Rediff patches with gbp pq
  * Update maintainer information
  * Update copyright information
  * Upgrade debhelper compatibility level
  * Update Vcs and Homepage info
  * Change priority extra to optional
  * Use autoreconf
  * Rediff patches and pulled upstream fixes (Closes: #919783)
  * Use python3
  * Delete unnecessary files
  * Add gawk as Build-Depends
  * Remove ppc64 32b and dbg packages
  * Use multiarch paths for libs
  * Refactor rules using current packaging facilities
  * Inherit of section for libdfp1
  * Add Multi-Arch information
  * Fix packages long descriptions
  * Fix Format URL
  * Update FSF address
  * Remove duplicate license description
  * Enable hardened build
  * Add initial symbol file
  * Avoid copying COPYING.txt
  * Remove Suggests on dbg package
  * Update Source URL
  * Make int128 related symbols optional
  * Add powerpc architecture
  * Bump Standards-Version

 -- Frédéric Bonnard <frediz@debian.org>  Thu, 31 Jan 2019 16:53:12 +0100

libdfp (1.0.13-1) unstable; urgency=medium

  * Upload to Debian Closes: #781530
  * Set Frederic as Maintainer
  * Set doko and I as Uploaders

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Fri, 25 Nov 2016 15:55:00 +0000

libdfp (1.0.13-0ubuntu1) yakkety; urgency=medium

  * New upstream release
  * Resolves optimised build failure on s390x
  * Set march to zEC12 on s390x

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Fri, 19 Aug 2016 10:15:26 +0100

libdfp (1.0.12-0ubuntu1) xenial; urgency=medium

  * New upstream release LP: #1544100
  * Add watch file
  * Add s390x build
  * Bump cpu to power8
  * Do not set cpu flat for s390x, FTBFS
  * Import upstream patches from master up to 1e53b39d

 -- Dimitri John Ledkov <xnox@ubuntu.com>  Wed, 17 Feb 2016 14:01:26 +0000

libdfp (1.0.11-0ubuntu2) wily; urgency=medium

  * Fix the usage of mfcr extended mnemonic. LP: #1459691.

 -- Matthias Klose <doko@ubuntu.com>  Fri, 24 Jul 2015 15:37:38 +0200

libdfp (1.0.11-0ubuntu1) wily; urgency=medium

  * libdfp 1.0.11 release.

 -- Matthias Klose <doko@ubuntu.com>  Wed, 22 Jul 2015 21:49:52 +0200

libdfp (1.0.9-0ubuntu1) vivid; urgency=medium

  * libdfp 1.0.9 release.
  * Build for ppc64el.

 -- Matthias Klose <doko@ubuntu.com>  Mon, 02 Feb 2015 23:47:46 +0100

libdfp (1.0.7-0ubuntu2) natty; urgency=low

  * Unmangle copyright file.

 -- Matthias Klose <doko@ubuntu.com>  Thu, 24 Feb 2011 23:48:49 +0100

libdfp (1.0.7-0ubuntu1) natty; urgency=low

  * Initial release, based on r12799.

 -- Matthias Klose <doko@ubuntu.com>  Wed, 23 Feb 2011 11:55:06 +0100
